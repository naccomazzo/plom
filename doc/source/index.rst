.. Plom documentation main file
   Copyright 2020-2022 Colin B. Macdonald
   SPDX-License-Identifier: AGPL-3.0-or-later

################################
Welcome to Plom's documentation!
################################

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   getting_started.rst

   running_a_server.rst

   preparing_an_exam.rst

   scanning

   unknown_pages

   identifying_papers.rst

   solutions.rst

   returning.rst

   cmdline.rst

   api.rst

   code.rst

   install.rst

   faq.md

   changelog.md


##################
Indices and tables
##################

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
