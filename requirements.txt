# SPDX-License-Identifier: FSFAP
# Copyright (C) 2020 Michael Zhang
# Copyright (C) 2020-2022 Colin B. Macdonald
# Copyright (C) 2020 Vala Vakilian
# Copyright (C) 2022 Elizabeth Xiao
#
# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.  This file is offered as-is,
# without any warranty.

# Not really server deps
PyQt5
PyQt5-sip

aiohttp==3.8.1
appdirs==1.4.4
arrow==1.2.2
canvasapi==2.2.0
exif==1.3.5
file-magic==0.4.0
imutils==0.5.4
lapsolver==1.1.0
numpy==1.23.2
opencv-python-headless==4.6.0.66
packaging==21.3
pandas==1.4.3
passlib==1.7.4
peewee==3.15.1
Pillow==9.2.0
pymupdf==1.20.2
pyzbar==0.1.9
requests==2.28.1
requests-toolbelt==0.9.1
scikit-learn==1.1.2
segno==1.5.2
stdiomask==0.0.6
toml==0.10.2
tqdm==4.64.0
urllib3==1.26.11
weasyprint==56.1
